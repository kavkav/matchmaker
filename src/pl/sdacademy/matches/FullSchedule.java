package pl.sdacademy.matches;

import java.util.*;

public class FullSchedule extends Round {
    private List<List<Match>> schedule;
    private List<String> didntPlayOnce = new ArrayList<>();
    Map timesTheTeamDidntPlay;

    public void create(String[] list) {
        //List<Round> schedule = new ArrayList<>();
        List<Match> listOfAllMatches = listOfAllPossibleMatches(list);
        Round round;
        Random r = new Random();
        Match drawnMatch;
        int random;

        round = new Round();
        while (listOfAllMatches.size() > 0) {
            do {
                drawnMatch
                random = r.nextInt();
            } while (teamPlayedInThisRound(round, drawnMatch));
            round.addMatch(listOfAllMatches.get(random));
            listOfAllMatches.remove(random);

        }

    }

    private boolean teamPlayedInThisRound(Round thisRound, Match match) {
        for (Match matchInThisRound : thisRound.getRound()) {
            return hasSameTeam(match, matchInThisRound);
        }
        return false;
    }

    private Match drawnMatch(int index, List<Match>)

    @Override
    public String toString() {
        String string = "";
        for (int i = 0; i < schedule.size(); i++) {
            string += "Round " + (i + 1) + "\n" + schedule.get(i) + "\n\n";
        }
        return string;
    }
}
