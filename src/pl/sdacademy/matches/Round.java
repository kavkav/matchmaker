package pl.sdacademy.matches;

import java.util.ArrayList;
import java.util.List;

public class Round extends Match{
    private List<Match> round;

    public Round(){
        round = new ArrayList<>();
    }

    public void addMatch(Match match){
        round.add(match);
    }

    public List<Match> getRound() {
        return round;
    }

    public static List<Match> listOfAllPossibleMatches(String[] allTeams){
        List<Match> list = new ArrayList<>();
        for (int i = 0; i < allTeams.length; i++) {
            for (int j = i + 1; j < allTeams.length; j++) {
                list.add(new Match(allTeams[i], allTeams[j]));
            }
        }
        return list;
    }
}
