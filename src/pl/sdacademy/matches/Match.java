package pl.sdacademy.matches;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Match{
    private String team1;
    private String team2;

    public Match(String team1, String team2){
        this.team1 = team1;
        this.team2 = team2;
    }

    public Match(){

    }

    public String getTeam1() {
        return team1;
    }

    public String getTeam2() {
        return team2;
    }

    public boolean hasSameTeam(Match match1, Match match2){
        if(match1.getTeam1().equals(match2.getTeam1()) || match1.getTeam1().equals(match2.getTeam2()) ||
                match1.getTeam2().equals(match2.getTeam1()) || match1.getTeam2().equals(match2.getTeam2())){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public String toString() {
        return team1 + "  vs  " + team2;
    }
}
